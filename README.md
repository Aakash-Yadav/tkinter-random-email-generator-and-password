# Tkinter random email generator and password

random email generator and password using user name and last-name 



### Random Email Password Generator Using Tkinter

It is a tool that generates Email and passwords based on the given guidelines that you set to create an unpredictable strong password for your accounts. The Password generator tool creates a random and customized password for users that helps them to create a strong password that provides greater security.

### Tkinter 

Tk class is used to create a root window. The Tk function provides a window of GUI as well as provides so many features like setting the title, set the geometry of the GUI window.
In these steps, we are giving the title of the GUI root window. The title is defined about the project in one line. Tkinter provides many methods, one of them is the geometry() method.
Here in this line of code, we are working on the background color of the Tkinter GUI window.

### OutPut:
##### Given Data 
![alt text](https://xp.io/storage/1PoJ72Cf.png)

##### Random Generat Email and password:
![alt text](https://xp.io/storage/1PoNz1iS.png)




